import { Controller, Get } from "@nestjs/common";

@Controller()
export class AppController {
    @Get('/hithere')
    getRootRoute(){
        return 'Hi there!';
    }
}
